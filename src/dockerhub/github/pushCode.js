const { pushCode } = require('./repoUtil')

const run = async () => {
  await pushCode()
  setInterval(async () => {
    await pushCode()
  }, 40 * 60 * 1000)
}

run()
